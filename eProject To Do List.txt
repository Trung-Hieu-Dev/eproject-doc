Front End:
	1. Viết lại toàn bộ nội dung:
	- product-details.html
	- contact.html
	- about.html
	- 404.html
	- login.html
	- blog-details.html
	
	2. Chỉnh sửa phù hợp
	- home -> header, megamenu, navbar
	- cart.html
	- wishlist.html
	- checkout.html
	- my-account.html
	- product-sidebar.html

Back End:
	- slugs
	- đăng ký / đăng nhập
	- so sánh giá
	- đổ nội dung vào các trang:
			- home -> lấy nội dung của product-details.html
			- cart.html -> nhận dữ liệu khi submit nút add-to-card
			- wishlist.html (so sánh giá) lấy nội dung khi submit nút wishlist
			- checkout.html -> lấy nội dung từ cart.html
			- my-account.html -> lấy nội dung từ login.html
			- product-sidebar.html -> lấy nội dung từ product-details.html
			- blog.html -> lấy nội dung từ blog-details.html

SQL:
	- Nhập dữ liệu sample data
	- Kiểm tra sửa đổi tên bảng & nội dung tương ứng.
	


Yêu cầu đề bài thi:
A. Yêu cầu chung:
	1. Bộ sưu tập thời trang đa dạng mẫu mã
	2. Bộ sưu tập phụ kiện
	3. Bộ sưu tập brands
	4. Chạy được trên nhiều trình duyệt

B. Yêu cầu chi tiết:
	1. Trang chủ bố cục các phần phù hợp, có logo thiết kế
	2. Bộ sưu tập phân loại theo nhà thiết kế, loại trang phục
	3. Giao diện thiết kế UX & UI
	4. Navbar di chuyển mượt mà
	5. So sánh các thương hiệu khác nhau (so sánh giá ?)
	6. Bộ sưu tập nổi bật
	7. Có trang about us, contact us (địa chỉ , bản đồ)
	8. Tất cả hình ảnh đều phải có thẻ alt
